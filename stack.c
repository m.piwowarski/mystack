#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

/* The function creates a stack structure object and sets its initial content */
Stack *stack_init(void){
    Stack *sp = (Stack*)malloc(sizeof(Stack));
    if(sp == NULL){
        // fujitsu: wytłumacz dlaczego używasz tu fprintf. I generalnie po co tej funkcji się używa.
        //          Co to jest stderr.
        /* funkcja fprintf służy do pisania do plilu, jej pierwszym argumentem jest wskaźnik plikowy.
         * Program standardowo otwiera trzy wskaźniki do plików stdin, stdout i stderr.
         * stderr to standardowy strumień błędów. Domyślnie standardowy strumień błędów jest wyświetlany w konsoli.
         * Użyłem fprintf zamiast printf ponieważ gdzieś przeczytałem, że błędy powinno sie wysyłać na
         * stderr a dane na stdout */
        fprintf(stderr, "Memory not allocated.\n");
        exit(1);
    }
    sp->array = (int*) malloc(sizeof(int) * INIT_STACK_CAPACITY);
    if(sp->array == NULL){
        fprintf(stderr, "Memory not allocated.\n");
        exit(1);
    }
    sp->top = STACK_EMPTY;
    sp->capacity = INIT_STACK_CAPACITY;
    sp->init_capacity = INIT_STACK_CAPACITY;
    return sp;
}

/* Function pushes the item on to the stack and if necessary increases its capacity */
void push(Stack *sp, int value){
    if(sp == NULL){
        fprintf(stderr, "Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    if(sp->top + 1  == sp->capacity){
        int *temp = (int*) realloc(sp->array, sizeof(int) * sp->capacity * CAPACITY_INCREASE_FACTOR );
        if(temp != NULL){
            sp->array = temp;
            sp->capacity *= CAPACITY_INCREASE_FACTOR;
        }
        else{
            // fujitsu: jeśli realloc się nie powiedzie czy trzeba zwalniać jakąś pamięć?
            printf("Memory not allocated.\n");
            // fujitsu: a co z pamięcia wskazywaną przes wskaźnik sp, czy trzeba ja zwalniać tu?
            /* Tak, w przypadku gdy realloc się nie powiedzie trzeba zwolnić pamięć dla tablicy i dla pamięci wskazywanej przes wskaźnik sp*/
            free(sp->array);
            free(sp);
            exit(1);
        }
    }
    sp->array[++sp->top] = value;
}

/* Function decrements the top index of the stack and reduces its capacity if needed */
PopValue pop(Stack *sp){
    PopValue r;
    r.value = 0;
    r.is_empty = 0;
    if(sp == NULL){
        fprintf(stderr, "Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    if(sp->top == STACK_EMPTY){
        r.is_empty = 1;
        return r;
    }
    else{
        // fujitsu: kiedy dokładnie ten if powinien się wykonywać? Nie do końca go rozumiemy
        /*  Tutaj chodzi o zmniejszenie pojemności stosu gdy są zdejmowane elementy ze stosu.
         *  np. jak stos ma pojemność 80 żeby przy zdjęciu 20 elementu jego pojemność się zmniejszyła do 40.
         *  gdy wskaźnik stosu top spadnie do 1/4 capacity i jednocześnie top nie bedzie mniejszy niż 10.
         *  Początkowo miałem tu 1/2 capacity ale przy 1/2 jest zwiększanie pojemności i gdyby ktoś
         *  na zmianę zdejmował element i odkładał to by ciągle wykonywała sie realokacjia pamieci.
         *  Uznałem, że to byłoby źle i zrobiłem 1/4. */
        if(sp->top + 1  == sp->capacity / CAPACITY_DENOMINATOR && sp->top + 1 >= sp->init_capacity){
            int *temp = (int*) realloc(sp->array, sizeof(int) * (sp->capacity / CAPACITY_DECREASE_FACTOR));
            if(temp != NULL){
                sp->array = temp;
                sp->capacity /= CAPACITY_DECREASE_FACTOR;
            }
            else{
                fprintf(stderr, "Memory not allocated.\n");
                free(sp->array);
                free(sp);
                exit(1);
            }
        }
        r.value = sp->array[sp->top--];
        return r;
    }
}

/* Function displays all items on the stack */
void display(const Stack *sp){
    if(sp == NULL){
        fprintf(stderr, "Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    if(sp->top == -1){
        printf("Stack is empty\n");
    }
    else{
        int i;
        for(i=sp->top; i>=0; i--)
            printf("%d\n",sp->array[i]);
    }
}
/* Function releases the allocated memory for the stack */
void stack_exit(Stack **sp){
    // fujitsu: dlaczego do tej funkcji przekazuje **sp, a nie mozna *sp?;
    /* Ponieważ w wywołaniu funkcji stack_exit przekazanie jej stack zamiast &stack spowodowałoby
     * utworzenie kopii adresu do obiektu struktury, wtedy NULL byłby przypisywany do kopi.
     * Spowodowałoby to, że zmienna wskaznikowa stack nie byłaby ustawiona na NULL i przy dwukrotnym wywołaniu stack_exit
     * wystąpiłby błąd powtórnego zwalniania pamięci.
     * Natomiast przekazanie &stack daje możliwość przypisanie NULL do wartosci spod adresu czyli do orginału (*sp) = NULL. */
    if(sp == NULL || *sp == NULL){
        printf("Pointer to stack is NULL, initialize stack object.\n");
        exit(1);
    }
    free((*sp)->array);
    free((*sp));
    (*sp) = NULL;
}


