#ifndef STACK_H
#define STACK_H

#define INIT_STACK_CAPACITY 10
#define STACK_EMPTY -1
#define CAPACITY_INCREASE_FACTOR 2
#define CAPACITY_DECREASE_FACTOR 2
#define CAPACITY_DENOMINATOR 4

typedef struct{
    int is_empty;
    int value;
}PopValue;

typedef struct{
    int top;
    int capacity;
    int init_capacity;
    int *array;
}Stack;

Stack *stack_init(void);
void push(Stack *sp, int value);
PopValue pop(Stack *sp);
void display(const Stack *sp);
void stack_exit(Stack **sp);

#endif
