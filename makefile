CC = gcc

OBJ = main.o  stack.o

all: main

main.o: main.c stack.h
	$(CC) main.c -c -o main.o
stack.o: stack.c stack.h
	$(CC) stack.c -c -o stack.o

clean:
	rm -f *.o stack

main: $(OBJ)
	$(CC) $(OBJ) -o stack
	
