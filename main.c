#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main()
{
    char ch;
    int choice, value, loop = 1;
    PopValue  pop_value;
    Stack *stack= stack_init();

    // fujitsu: co sie stanie jak stack_init zwróci NULL
    /* Zmienna wskaźnikowa stack nie bedzie wskazywała na żaden adres.
     *  W dalszej części programu wywołane funkcje stosu wejdą
     *  w obsługę błedu od wskaźnika równego NULL. */

    while(loop){
        printf("\n***** MENU *****\n");
        printf(" 1. Push\n 2. Pop\n 3. Display\n 4. Exit\n");
        printf("\nEnter your choice: ");

        choice = 0;

        if(!(scanf("%d", &choice))){
            fprintf(stderr, "Choice should be integer number\n");
            // fujitsu: wytłumacz co dokładnie robi poniższa pętla loop i czy ona jest naprawdę potrzebna
            /* Ta pętla usuwa wszystkie znaki z bufora wejściowego. Scanf pozostawia w buforze nieprawidłowo
             * wprowadzony znak (np. char a). Mogłaby być pojedyńcz funkcj getchr(), ale zastosowanie pętli
             * zapobiega też kilkukrotnemu wyświetleniu się menu w przypadku wpisania kilku
             * nieprawidłowych znaków np. aaa . */
            //printf("ch = %d ", getchar());
            while(getchar() != '\n')
                continue;

        }
        else{
            switch(choice){
            case 1:
                printf("Enter the value to be insert: ");
                scanf("%d",&value);
                push(stack, value);
                break;
            case 2:
                pop_value = pop(stack);
                if(pop_value.is_empty)
                    printf("Stack is empty\n");
                else
                    printf("Value taken from the top of the stack: %d\n", pop_value.value);
                printf("stack capacity: %d\n", stack->capacity);
                break;
            case 3:
                display(stack);
                break;
            case 4:
                printf("Do you want to finish? y = yes, n = no \n");
                // fujitsu: dlaczego tu najpierw używasz getchar a później scanf. Np przy wyborze menu używasz tylko scanf.
                /* Ponieważ scanf w menu pozostawia w buforze wejsciowym znak nowego wiersza,
                 *  getchar() go pobiera i porzuca (czyli usuwa z bufora) */
                printf("ch = %d ", getchar());
                scanf("%c", &ch);
                if(ch == 'y'){
                    stack_exit(&stack);
                    loop = 0;
                }
                break;
            default:
                printf("\nWrong selection!!! Try again!!!");
                break;
            }
            // fujitsu: wytłumacz co dokładnie robi poniższa pętla loop i czy ona jest naprawdę potrzebna
            while(getchar() != '\n')
                continue;
        }
    }
    return 0;
}

